# CatDive

A fun visualization of cat images pulled from [infinitecat.com](http://infinitecat.com) that uses [Feature Matching with OpenCV](https://docs.opencv.org/4.x/dc/dc3/tutorial_py_matcher.html) and [CSS Transformation Matrices](http://jsfiddle.net/dFrHS/1/).

`npm start` will run the vite dev server http://localhost:5173/
